package labyrinth;

import java.awt.Color;

import datastructure.GridDirection;
import datastructure.IGrid;
import datastructure.CellPosition;

public class Labyrinth implements ILabyrinth {

	private final IGrid<LabyrinthTile> tiles;
	private CellPosition playerPos;

	boolean playerSet;

	public Labyrinth(IGrid<LabyrinthTile> tiles) throws LabyrinthParseException {
		if (tiles == null) {
			throw new IllegalArgumentException();
		}

		this.tiles = tiles;

		int numPlayers = 0;
		for (CellPosition pos : tiles.cellPositions()) {
			if (tiles.get(pos) == LabyrinthTile.PLAYER) {
				numPlayers++;
				playerPos = pos;
				playerSet = true;
			}
		}
		if (numPlayers != 1) {
			throw new LabyrinthParseException("Labyrinth created with " + numPlayers + " number of players!");
		}

		checkState(this);
	}

	public static void checkState(Labyrinth labyrinth) {
		boolean ok = !labyrinth.playerSet || labyrinth.isValidPos(labyrinth.playerPos);
		int numPlayers = 0;
		for (CellPosition pos : labyrinth.tiles.cellPositions()) {
			if (labyrinth.tiles.get(pos) == LabyrinthTile.PLAYER) {
				numPlayers++;
			}
		}
		if (labyrinth.playerSet) {
			ok &= numPlayers == 1;
		} else {
			ok &= numPlayers == 0;
		}
		if (!ok) {
			throw new IllegalStateException("bad object");
		}
	}

	@Override
	public LabyrinthTile getCell(CellPosition pos) {
		checkPosition(pos);

		return tiles.get(pos);
	}

	@Override
	public Color getColor(CellPosition pos) {
		if (!isValidPos(pos)) {
			throw new IllegalArgumentException("Location invalid");
		}

		return tiles.get(pos).getColor();
	}

	@Override
	public int numberOfRows() {
		return tiles.rows();
	}

	@Override
	public int getPlayerGold() {
		return 0;
	}

	@Override
	public int getPlayerHitPoints() {
		return 0;
	}

	@Override
	public int numberOfColumns() {
		return tiles.cols();
	}

	@Override
	public boolean isPlaying() {
		return playerSet;
	}

	private boolean isValidPos(CellPosition pos) {
		return pos.row() >= 0 && pos.row() < tiles.rows() //
				&& pos.col() >= 0 && pos.col() < tiles.cols();
	}

	private void checkPosition(CellPosition pos) {
		if (!isValidPos(pos)) {
			throw new IndexOutOfBoundsException("Row and column indices must be within bounds");
		}
	}

	@Override
	public void movePlayer(GridDirection d) throws MovePlayerException {
			// ...
		  
		  
		if(!playerCanGo(d)){
			throw new MovePlayerException("WRONG MOVE!");
		}
		CellPosition newPos = playerPos.getNeighbor(d);
		tiles.set(playerPos, LabyrinthTile.OPEN);
		playerPos = newPos;
		tiles.set(newPos, LabyrinthTile.PLAYER);

		checkState(this);
	}

	@Override
	public boolean playerCanGo(GridDirection d) {
		if (d == null) {
			throw new IllegalArgumentException();
		}

		return playerCanGoTo(playerPos.getNeighbor(d));
	}

	/**
	 * This method checks if a player can move to a given location
	 * A player can not go to the location if there is a wall or
	 * if the location is outside the bounds of the grid.
	 * 
	 * @param pos the position
	 * @return true if player can move, false otherwise
	 */
	private boolean playerCanGoTo(CellPosition pos) {
		if (!isValidPos(pos)) {
			return false;
		}

		return tiles.get(pos) != LabyrinthTile.WALL;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int y = tiles.cols() - 1; y >= 0; y--) {
			for (int x = 0; x < tiles.rows(); x++) {
				sb.append(getSymbol(new CellPosition(x, y)));
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	/**
	 * No bounds checking will be done for the given {@code pos}.
	 */
	private String getSymbol(CellPosition pos) {
		return String.valueOf(tiles.get(pos).getSymbol());
	}

	@Override
	public Iterable<CellPosition> coordinates() {
		return tiles.cellPositions();
	}
}
